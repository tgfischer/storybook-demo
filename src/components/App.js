import React from "react";
import { Container } from "semantic-ui-react";
import styled from "styled-components";

import DataTable from "./DataTable";

const Wrapper = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const columns = [
  {
    key: "name",
    label: "Name"
  },
  {
    key: "email",
    label: "Email"
  },
  {
    key: "number",
    label: "Number"
  }
];

const rows = [
  {
    name: "Tom Fischer",
    email: "tom.fischer@magnetforensics.com",
    number: 5
  },
  {
    name: "Andrew Welton",
    email: "andrew.welton@magnetforensics.com",
    number: 78
  },
  {
    name: "Andrew Vanderveen",
    email: "andrew.vanderveen@magnetforensics.com",
    number: 2
  },
  {
    name: "Mike Parkhill",
    email: "mike.parkhill@magnetforensics.com",
    number: 18
  },
  {
    name: "Gaya Thananjagen",
    email: "gaya.thananjagen@magnetforensics.com",
    number: 134
  },
  {
    name: "Cam Sapp",
    email: "cam.sapp@magnetforensics.com",
    number: 56
  },
  {
    name: "William Lindsay",
    email: "william.lindsay@magnetforensics.com",
    number: 32
  },
  {
    name: "Jeffrey Kim",
    email: "jeffrey.kim@magnetforensics.com",
    number: 10
  },
  {
    name: "Kieran Cunney",
    email: "kieran.cunney@magnetforensics.com",
    number: 98
  },
  {
    name: "Collin McMillan",
    email: "collin.mcmillan@magnetforensics.com",
    number: 41
  }
];

const App = () => (
  <Wrapper>
    <Container>
      <DataTable columns={columns} rows={rows} />
    </Container>
  </Wrapper>
);

export default App;
