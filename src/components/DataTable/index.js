import React from "react";
import PropTypes from "prop-types";
import identity from "lodash/identity";
import { Table } from "semantic-ui-react";

import { directions } from "../../constants";
import useDataTable from "./useDataTable";

const DataTable = props => {
  const { rows, column, direction, sort } = useDataTable(
    props.rows,
    props.column,
    props.direction
  );
  return (
    <Table celled sortable>
      <Table.Header>
        <Table.Row>
          {props.columns.map(({ key, label }) => (
            <Table.HeaderCell
              key={key}
              sorted={column === key ? direction : undefined}
              onClick={() => {
                props.onSort(key, direction);
                sort(key);
              }}
            >
              {label}
            </Table.HeaderCell>
          ))}
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {rows.map((row, i) => (
          <Table.Row key={i}>
            {Object.keys(row).map(key => (
              <Table.Cell key={key}>{row[key]}</Table.Cell>
            ))}
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  );
};

DataTable.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  rows: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
  column: PropTypes.string,
  direction: PropTypes.oneOf([directions.asc, directions.desc]),
  onSort: PropTypes.func
};

DataTable.defaultProps = {
  onSort: identity
};

export default DataTable;
