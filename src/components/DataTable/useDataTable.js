import { useEffect, useState } from "react";
import orderBy from "lodash/orderBy";
import findKey from "lodash/findKey";

import { directions } from "../../constants";

export default (rows, column, direction) => {
  const [state, setState] = useState({ rows, column, direction });

  useEffect(() => {
    setState({
      ...state,
      rows: orderBy(
        state.rows,
        [state.column],
        findKey(directions, value => value === state.direction)
      )
    });
  }, [state.column, state.direction]);

  return {
    ...state,
    sort: column => {
      let direction = directions.asc;
      if (state.column === column) {
        direction =
          state.direction === directions.asc ? directions.desc : directions.asc;
      }
      return setState({ ...state, column, direction });
    }
  };
};
