import React from "react";
import { shallow } from "enzyme";

import DataTable from "../index";

describe("DataTable tests", () => {
  it("should render the DataTable", () => {
    const wrapper = shallow(
      <DataTable
        columns={[
          {
            key: "one",
            label: "Column One"
          },
          {
            key: "two",
            label: "Column Two"
          },
          {
            key: "three",
            label: "Column Three"
          }
        ]}
        rows={[
          {
            one: "1, 1",
            two: "1, 2",
            three: "1, 3"
          },
          {
            one: "2, 1",
            two: "2, 2",
            three: "2, 3"
          },
          {
            one: "3, 1",
            two: "3, 2",
            three: "3, 3"
          }
        ]}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
